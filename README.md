# OPIS PLIKÓW

**Repozytorium zawiera**:

1. Niniejszą instrukcję;
1. Pliki zakończone na .php obrazujące kod odpowiadający za ścieżkę użytkownika, o której poniżej;
1. Plik Struktura_Plików.txt obrazujący pełną strukturę plików składających się na Platformę (Zamawiający pominął powtarzające się pliki svg/jpg/png, lokalizacyjne/językowe, oraz związane z bezpieczeństwem aplikacji)

**Ten dokument demonstruje zastosowany w aplikacji kod bazując na ścieżce użytkownika, który zgłasza się do poprzedzających konkurs konsultacji**.

*Nazwa pliku po prawej stronie od tekstu koresponsuje z nazwą w repozytorium.*

**Krok 1**: Instytucja ogłasza konsultacje, co aktywuje następującą funkcję w pliku kontrolującym: **edycja_konsultacji**


**Krok 2**: Uczestnik zgłasza się do konsultacji wypełniając formularz PHP ze zgłoszeniem do konsultacji: **zgloszenie_do_konsultacji_view**


**Krok 2.5**: Opcjonalnie może zamiast tego obserwować konsultacje, co aktywuje następującą funkcję w pliku kontrolującym: **obserwuj_konsultacje**


**Krok 3**: Instytucja zmienia status zgłoszenia na zaakceptowany/odrzucony/do uzupełnienia: **zmiana_statusu_view**


**Krok 4**: Historia zmian statusu jest dostępna dla uprawnionych użytkowników: **historia_zmian_statusu_view**


Postępowanie konkursowe można interpretować jako „rozszerzoną” wersję konsultacji, gdzie zawarte są m.in:

*	Dwa etapy
*	Moduł uploadu plików z pracami konkursowymi
*	Ocenę prac
*	Zachowanie dokumentacji postępowania.

Ich wdrożenie jest analogiczne do konsultacji – oparte na strukturze formularz PHP/plik kontrolujący i opisane w dokumentacji będącej załącznikiem do postępowania. 

*Niniejsze repozytorium jest udostępniane jedynie na potrzeby postępowania prowadzonego przez Naukową Akademicką Sieć Komputerową - Państwowy Instytut Badawczy (NASK-PIB). Całość kodu stanowi własność Skarbu Państwa - Ministra Cyfryzacji. Wszystkie pliki w repozytorium udostępniane sa jedynie na potrzeby postępowania. Ich inne wykoryzstanie wymaga osobnej zgody właściciela.*
